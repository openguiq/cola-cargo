package com.rickie.domain.model.valueobjects;

public enum RoutingStatus {

    NOT_ROUTED, ROUTED, MISROUTED;
}
